from django.shortcuts import render

from django.views.generic.base import TemplateView
from django.views.generic import DetailView
from django.views.generic import ListView

from .models import Blogdb,Anggota
# Create your views here.

class index(TemplateView):
    template_name = 'web/index.html'
    
    def get_context_data(self,**kwargs):
        context = super(index,self).get_context_data(**kwargs)
        context['lastfive'] = Blogdb.objects.order_by('-pk')[:5]
        return context
    
class blog(ListView):
    model = Blogdb
    template_name = 'web/update.html'
    
class blogpost(DetailView):
    model = Blogdb
    template_name = 'web/post.html'
    
class member(ListView):
    model= Anggota
    template_name = 'web/anggota.html'

class lisensi(TemplateView):
    template_name = 'web/lisensi.html'
