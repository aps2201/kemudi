from django.contrib import admin

# Register your models here.

from .models import Blogdb
from .models import Kategori
from .models import Anggota


@admin.register(Blogdb)
class blogdbAdmin(admin.ModelAdmin):
    exclude = ['fotofitur']

@admin.register(Kategori)
class kategoriAdmin(admin.ModelAdmin):
    pass

@admin.register(Anggota)
class anggotaAdmin(admin.ModelAdmin):
    list_display = ('nama','nomor',)
