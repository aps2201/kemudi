from django.db import models
from datetime import date
from ckeditor_uploader.fields import RichTextUploadingField
#from django.db.models import permalink
from django.urls import reverse
import uuid

# Create your models here.

class Blogdb(models.Model):
    judul = models.CharField(max_length = 50)
    konten = RichTextUploadingField()
    tanggal = models.DateTimeField('tanggal')
    dipost = models.DateField(db_index=True, auto_now_add=True)
    kategori = models.ForeignKey('web.Kategori',null=True,on_delete=models.CASCADE)
    slug = models.SlugField(max_length=100, unique=True,null=True)
    fotofitur = models.FileField(upload_to="upload/fitur/",blank=True)
    
    def __str__(self):
        return self.judul
    
    def get_absolute_url(self):
        return reverse('web:blog-post', args=[str(self.slug)])
    class Meta:
        verbose_name_plural = "Blog Content"
        ordering = ['-tanggal']


class Kategori(models.Model):
    kategori = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(max_length=100, db_index=True)
    
    def __str__(self):
        return self.kategori
    
#    @permalink
    def get_absolute_url(self):
        return reverse('web:kategori', args=[str(self.id)])
    class Meta:
        verbose_name_plural = "List Kategori"

class Anggota(models.Model):
    nama = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    pgp = models.TextField(max_length=5000,null=True,blank=True)
    nomor = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    
