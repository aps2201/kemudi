
from django.urls import path

from . import views

app_name = 'web'
urlpatterns = [
    path('', views.index.as_view(), name='index'),
    path('blog/', views.blog.as_view(), name='blog'),
    path('blog/post/<slug:slug>', views.blogpost.as_view(), name='blog-post'),
    path('member/', views.member.as_view(), name='member'),
    path('atribut_lisensi/', views.lisensi.as_view(), name='lisensi'),
]
