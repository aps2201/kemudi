"""kemudi_web_v2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path
from django.urls import include

from django.contrib import admin
from django.views.generic import RedirectView
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView

import django_pydenticon.urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('kemudi/', include('web.urls',namespace = 'web')),
    path('', RedirectView.as_view(url='/kemudi/', permanent=False)),
    path('accounts/', include('django.contrib.auth.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('identicon/',django_pydenticon.urls.get_patterns()),
    #url(r'^blog/category/(?P<slug>[^\.]+).html','web.blogpost.views.view_category',name='view_blog_category'),

]
